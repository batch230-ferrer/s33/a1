fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);

});

//////

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(response => console.log(response));

////////

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers:{
		"Content-Type" : "application/json"
	},
	body:JSON.stringify({
		complpeted: false,
		id: 201,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

/////////

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
        "Content-Type" : "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "Pending",
        description: "To update the my to do list with a different data structure",
        status: "Pending",
        title: "Updated To Do List Item",
        userID: 1   
    })

})
.then(response => response.json())
.then(json => console.log(json))

////////

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: {
        "Content-Type" : "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "07/09/21",
        status: "Complete",
    })
})
.then(response => response.json())
.then(json => console.log(json));

/////////

fetch("http://jsonplaceholder.typicode.com/posts/1", 
    {
        method: "DELETE"
    }
).then(response => response.json())
.then(json => console.log(json))